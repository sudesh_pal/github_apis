__author__ = 'sudesh'
import pytest
from mixer.backend.django import mixer
pytestmark = pytest.mark.django_db


class TestUser:

    def test_model(self):
        user = mixer.blend("users.User", name="sudesh")
        assert user.name == "sudesh", 'Should create a new user instance'
