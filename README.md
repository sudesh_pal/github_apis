# Github Apis

### Getting Started

    These instructions will get you a copy of the project up and running on your local machine
    for development and testing purposes.

### Prerequisites

    python3.5.5

### Installation

* clone the project

```shell
git clone git@gitlab.com:sudesh_pal/github_apis.git
cd github_apis
```

* Create new virtualenv

```shell
virtualenv .env3
source .env3/bin/activate
```

* Install dependencies

```shell
pip install -r requirements.txt
```

* Install redis-server

```shell
sudo apt install redis-server
```

* Start redis-server

```shell
sudo service redis-server start
```

* Install supervisor

```shell
sudo apt install supervisor
```

* Add celery task to supervisor

```shell
sudo vim /etc/supervisor/conf.d/github_api_celery.conf
```

Copy following configuration in this file


```
[program:github_apis-celery]
command=/home/{path to github_apis directory}/.env3/bin/celery --app=github_apis.celery:app worker --loglevel=INFO
directory={path to github_apis directory}/
user={username}
numprocs=1
stdout_logfile={path to github_apis directory}/logs/celery-worker.log
stderr_logfile={path to github_apis directory}/logs/celery-worker.log
autostart=true
autorestart=true
startsecs=10

; Need to wait for currently executing tasks to finish at shutdown.
; Increase this if you have very long running tasks.
stopwaitsecs = 600

; When resorting to send SIGKILL to the program to terminate it
; send SIGKILL to its whole process group instead,
; taking care of its children as well.
killasgroup=true

; if rabbitmq is supervised, set its priority higher
; so it starts first
priority=998

```

* Start celery tasks:

```shell
sudo supervisorctl reread
sudo supervisorctl update
sudo supervisorctl restart all
```

* Create database

```shell
cd {path to project home}
source .env3/bin/activate
./manage.py migrate
```

* Create django superuser

```shell
./manage.py createsuperuser
```

* Start django server

```shell
./manage.py runserver
```

* Open following link in browser

```
http://localhost:8000
```

* Admin login

```
http://localhost:8000/admin
```