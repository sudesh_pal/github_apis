from github_apis.celery import app
from .models import User
from .serializers.user_serializer import UserSerializer

# @app.task
# def add(x, y):
#     return x + y

@app.task
def save_users(result):
    result_dict ={}
    for row in result["items"]:
        result_dict[row.get("id")] = row

    existing_users = User.objects.filter(id__in=result_dict.keys())
    users = []

    for user in existing_users:
        serializer= UserSerializer(user, data=result_dict.pop(user.id))
        if serializer.is_valid():
            updated_user = serializer.save()
            users.append(updated_user)
        else:
            print(serializer.errors)
            print("User data is not valid.")

    for id, row in result_dict.items():
        serializer= UserSerializer(data=row)
        if serializer.is_valid():
            updated_user = serializer.save()
            users.append(updated_user)
        else:
            print(serializer.errors)
            print("User data is not valid.")