from django.db import models
from django.utils import timezone


class User(models.Model):
    id              = models.IntegerField(db_index=True, primary_key=True)
    login           = models.CharField(db_index=True,max_length=225)
    avatar_url      = models.URLField(max_length=500, null=True)
    gravatar_id     = models.CharField(max_length=225, null=True, blank=True)
    url             = models.URLField(max_length=500, null=True)
    html_url        = models.URLField(max_length=500, null=True)
    followers_url   = models.URLField(max_length=500, null=True)
    following_url   = models.URLField(max_length=500, null=True)

    gists_url           = models.URLField(max_length=500, null=True)
    starred_url         = models.URLField(max_length=500, null=True)
    subscriptions_url   = models.URLField(max_length=500, null=True)
    organizations_url   = models.URLField(max_length=500, null=True)
    repos_url           = models.URLField(max_length=500, null=True)
    events_url          = models.URLField(max_length=500, null=True)
    received_events_url = models.URLField(max_length=500, null=True)

    type            = models.CharField(max_length=225, null=True)
    site_admin      = models.BooleanField(default=False)
    name            = models.CharField(max_length=225, null=True)
    company         = models.CharField(max_length=225, null=True)
    blog            = models.URLField(max_length=500, null=True)
    location        = models.CharField(max_length=500, null=True)
    email           = models.EmailField(max_length=500, null=True)
    hireable        = models.CharField(max_length=225, null=True)
    bio             = models.TextField(null=True)
    public_repos    = models.IntegerField(default=0)
    public_gists    = models.IntegerField(default=0)
    followers       = models.IntegerField(default=0)
    following       = models.IntegerField(default=0)
    score           = models.FloatField(default=0)
    created_at      = models.DateTimeField(auto_now_add=True)
    updated_at      = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return "user "+str(self.login)
