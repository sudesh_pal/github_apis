__author__ = 'sudesh'

from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^$', views.ListUserView.as_view()),
    url(r'^(?P<pk>[0-9]+)/$', views.RetrieveUpdateDestroyUserView.as_view()),
]
