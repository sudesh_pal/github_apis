
from django.conf.urls import url, include
from django.contrib import admin
from users.urls import urlpatterns as user_urls

from users.views import IndexView

from django.conf import settings
admin.site.site_header = settings.ADMIN_SITE_HEADER

urlpatterns = [
    url(r'^$', IndexView.as_view()),
    url(r'^admin/', admin.site.urls),
    url(r'^advanced_filters/', include('advanced_filters.urls')),
    url(r'^user/', include(user_urls)),

]
