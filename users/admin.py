from django.contrib import admin
from django.http import HttpResponse
import xlwt
from .models import User
from django.utils import timezone


from urllib.parse import urlencode
from django.shortcuts import redirect

from advanced_filters.admin import AdminAdvancedFiltersMixin

class DefaultFilterMixin:
    def get_default_filters(self, request):
        raise NotImplementedError()


def changelist_view(self, request, extra_context=None):
    ref = request.META.get('HTTP_REFERER', '')
    path = request.META.get('PATH_INFO', '')
    if request.GET or ref.endswith(path):
        return super().changelist_view(
            request,
            extra_context=extra_context
        )
    query = urlencode(self.get_default_filters(request))
    return redirect('{}?{}'.format(path, query))


def export_xls(modeladmin, request, queryset):
    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename=mymodel.xls'
    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet("MyModel")

    row_num = 0

    columns = [
        (u"ID", 2000),
        (u"Login", 6000),
        (u"Score", 8000)
    ]

    font_style = xlwt.XFStyle()
    font_style.font.bold = True

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num][0], font_style)
        # set column width
        ws.col(col_num).width = columns[col_num][1]

    font_style = xlwt.XFStyle()
    font_style.alignment.wrap = 1

    for obj in queryset:
        row_num += 1
        row = [
            obj.pk,
            obj.login,
            obj.score,
        ]
        for col_num in range(len(row)):
            ws.write(row_num, col_num, row[col_num], font_style)

    wb.save(response)
    return response

export_xls.short_description = u"Export XLS"


class UserAdmin(DefaultFilterMixin,AdminAdvancedFiltersMixin, admin.ModelAdmin):

    date_hierarchy = 'created_at'

    def get_default_filters(self, request):
        now = timezone.now()
        return {
            'created_at__year': now.year,
            'created_at__month': now.month,
        }

    advanced_filter_fields = (
        'login', 'score', 'created_at'
        #'country__name', 'posts__title', 'comments__content'
    )


    
    def user_image_avatar(self, obj):
        return '<img src="%s" style="height:40px"/>' % obj.avatar_url

    def user_image_full(self, obj):
        return '<img src="%s" style="height:200px"/>' % obj.avatar_url

    user_image_avatar.allow_tags = True
    user_image_full.allow_tags = True

    list_display = ('user_image_avatar','login', 'score', 'created_at')
    search_fields = ['login']
    list_per_page = 15

    readonly_fields = ('user_image_full',"id")

    actions = [ export_xls,]




admin.site.register(User, UserAdmin)
