__author__ = 'sudesh'

from django.conf import settings
import requests
from urllib.parse import urljoin
from users.tasks import save_users


def serach_users(querystring,page):
    if not querystring or len(querystring) == 0:
        return None, None

    params = {"q":querystring,"page":page}
    headers = {'Accepts': 'application/vnd.github.v3.text-match+json'}
    url = urljoin(settings.GITHUB_API_URL, "search/users")
    res = requests.get(url, params=params, headers=headers)

    if res.status_code != 200:
        return None, None
    result = res.json()
    save_users.delay(result)

    return result['items'], result["total_count"]




