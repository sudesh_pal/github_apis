from django.views.generic import TemplateView


# Create your views here.

from rest_framework.generics import ListAPIView, RetrieveUpdateDestroyAPIView
from .models import User
from .serializers.user_serializer import UserSerializer
from .helpers import serach_users
from rest_framework.response import Response
from rest_framework import status


class ListUserView(ListAPIView):
    serializer_class = UserSerializer
    # queryset = User.objects.all()

    def get(self, request, *args, **kwargs):
        users, total = serach_users(self.request.query_params.get("q",None), self.request.query_params.get("page",1))
        if users:
            return Response({"users":self.get_serializer_class()(users,many=True).data, "total":total})
        return Response({"detail":"No result found"}, status=status.HTTP_404_NOT_FOUND)

    def get_queryset(self):
        users, total = serach_users(self.request.query_params.get("q",None))
        return users


class RetrieveUpdateDestroyUserView(RetrieveUpdateDestroyAPIView):
    serializer_class = UserSerializer
    queryset = User.objects.all()


class IndexView(TemplateView):
    template_name = 'index.html'
